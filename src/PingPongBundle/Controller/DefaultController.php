<?php

namespace PingPongBundle\Controller;

use PingPongBundle\Entity\Game;
use PingPongBundle\Entity\GamePairs;
use PingPongBundle\Entity\Player;
use PingPongBundle\Entity\TournamentGame;
use PingPongBundle\Models\EloRating;
use PingPongBundle\Models\RecountRatings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller {
	protected $defaultRating = 1500;


	public function indexAction() {
		return $this->redirect($this->generateUrl('ping_pong_solo'));
	}

	public function soloAction(Request $request) {
		$fb = $this->createFormBuilder();
        $fb->add('winnerName', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"]]);
        $fb->add('loserName', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"]]);
        $champs = $this->getDoctrine()
                    ->getRepository('PingPongBundle:Champs')
                    ->findBy(array('status' => 1, 'gameType' => 0), array('id' => 'DESC'));
        if(!empty($champs)) {
            $champChoices = array();
            foreach($champs as $champ) {
                /* @var Champs $champ */
                $champChoices[$champ->getName()] = $champ->getId();
            }
            $fb->add('Champ', ChoiceType::class,
                [   'label_format' => '%name%',
                    'choices' => $champChoices,
                    'required'  => false,
                    'placeholder' => 'Choose championship',
                    'choice_translation_domain' => false,
                ]);
        }

        $fb->add('addGame', SubmitType::class, ['label_format' => '%name%',]);

        $form = $fb->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			if ($data['winnerName'] == $data['loserName']) {
				$twigArray['errorMessage'] = 'CantPlayYourself';
			} else {
				if (!$this->saveSoloGame($data)) {
					$twigArray['errorMessage'] = 'FailedSavingGame';
				} else {
					return $this->redirectToRoute('ping_pong_solo');
				}
			}
		}

		$twigArray['form'] = $form->createView();

		$playersAll = $this->getDoctrine()
			->getRepository('PingPongBundle:Player')
			->findAllSortedSolo(array('p.ratingSolo' => 'DESC'));
		if (!empty($playersAll)) {
            $players = [];
            $playersGuests = [];
            $playerNames = [];
            foreach($playersAll as $player) {
                $playerNames[] = $player['gPlayer']->getName();
                if ($player['gGamesLast'] > 0 && $player['gGames'] > 5) {
                    $players[] = $player;
                } elseif($player['gGames'] != 0) {
                    $playersGuests[] = $player;
                }
            }
			$twigArray['players'] = $players;
            $twigArray['playersGuests'] = $playersGuests;
            $twigArray['playerNames'] = $playerNames;
		}

		$games = $this->getDoctrine()
			->getRepository('PingPongBundle:Game')
			->getGameList();
		if (!empty($games)) {
			$twigArray['games'] = $games;
		}
		return $this->render('PingPongBundle:Default:solo.html.twig', $twigArray);
	}

	public function pairsAction(Request $request) {
		$form = $this->createFormBuilder()
			->add('winnerName1', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"],])
			->add('winnerName2', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"],])
			->add('loserName1', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"],])
			->add('loserName2', TextType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"],])
			->add('addGame', SubmitType::class, ['label_format' => '%name%', 'attr' => ["autocomplete" => "off"],])
			->getForm();

		$form->handleRequest($request);

		if ($form->isSubmitted() && $form->isValid()) {
			$data = $form->getData();
			if (!$this->checkNames(array(
				$data['winnerName1'],
				$data['winnerName2'],
				$data['loserName1'],
				$data['loserName2']
			))
			) {
				$twigArray['errorMessage'] = 'CantPlayYourself';
			} else {
				if (!$this->savePairsGame($data['winnerName1'], $data['winnerName2'], $data['loserName1'], $data['loserName2'])) {
					$twigArray['errorMessage'] = 'FailedSavingGame';
				} else {
					return $this->redirectToRoute('ping_pong_pairs');
				}
			}
		}

		$twigArray['form'] = $form->createView();


		$playersAll = $this->getDoctrine()
			->getRepository('PingPongBundle:Player')
			->findAllSortedPairs(array('p.ratingPairs' => 'DESC'));
		if (!empty($playersAll)) {
			$players = [];
			$playersGuests = [];
            $playerNames = [];
			foreach($playersAll as $player) {
                $playerNames[] = $player['gPlayer']->getName();
				if ($player['gGamesLast'] > 0 && $player['gGames'] > 5) {
					$players[] = $player;
				} elseif($player['gGames'] != 0) {
					$playersGuests[] = $player;
				}
			}

			$twigArray['players'] = $players;
			$twigArray['playersGuests'] = $playersGuests;
            $twigArray['playerNames'] = $playerNames;
		}

		$games = $this->getDoctrine()
			->getRepository('PingPongBundle:GamePairs')
			->findBy(array('deleted' => '0'), array('time' => 'DESC'));
		if (!empty($games)) {
			$twigArray['games'] = $games;
		}

		return $this->render('PingPongBundle:Default:pairs.html.twig', $twigArray);
	}

	public function soloDeleteAction($id) {
		if (!is_numeric($id) || empty($id)) {
			return $this->redirect($this->generateUrl('ping_pong_solo'));
		}
		$repository = $this->getDoctrine()->getRepository('PingPongBundle:Game');
		/* @var Game $game */
		$game = $repository->find($id);
		if (empty($game)) {
			return $this->redirect($this->generateUrl('ping_pong_solo'));
		}
		$game->setDeleted(1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($game);
		$em->flush();
		$rc = new RecountRatings($this->getDoctrine(), new EloRating());
		$rc->recountSolo();

		return $this->redirect($this->generateUrl('ping_pong_solo'));
	}

	public function pairsDeleteAction($id) {
		if (!is_numeric($id) || empty($id)) {
			return $this->redirect($this->generateUrl('ping_pong_pairs'));
		}
		$repository = $this->getDoctrine()->getRepository('PingPongBundle:GamePairs');
		/* @var GamePairs $game */
		$game = $repository->find($id);
		if (empty($game)) {
			return $this->redirect($this->generateUrl('ping_pong_pairs'));
		}
		$game->setDeleted(1);
		$em = $this->getDoctrine()->getManager();
		$em->persist($game);
		$em->flush();
		$rc = new RecountRatings($this->getDoctrine(), new EloRating());
		$rc->recountPairs();
		return $this->redirect($this->generateUrl('ping_pong_pairs'));
	}

	public function recountSoloAction() {
		$rc = new RecountRatings($this->getDoctrine(), new EloRating());
		$rc->recountSolo();
		return $this->redirect($this->generateUrl('ping_pong_solo'));
	}

	public function recountPairsAction() {
		$rc = new RecountRatings($this->getDoctrine(), new EloRating());
		$rc->recountPairs();
		return $this->redirect($this->generateUrl('ping_pong_pairs'));
	}

	protected function checkNames($names) {
		while (!empty($names)) {
			$name = array_pop($names);
			if (in_array($name, $names)) {
				return false;
			}
		}
		return true;
	}

	protected function selectPlayer($playerName) {
		$repository = $this->getDoctrine()->getRepository('PingPongBundle:Player');
		$player = $repository->findOneBy(array('name' => $playerName));
		if (empty($player)) {
			$player = new Player();
			$player->setRatingSolo($this->defaultRating);
			$player->setRatingPairs($this->defaultRating);
			$player->setName($playerName);
			$em = $this->getDoctrine()->getManager();
			$em->persist($player);
			$em->flush();
		}
		return $player;
	}

	protected function saveSoloGame($data) {
        $winnerName = $data['winnerName'];
        $loserName = $data['loserName'];
		if (empty($loserName) || empty($winnerName) || $winnerName == $loserName) {
			return false;
		}
		$rCalc = new EloRating();
		$winner = $this->selectPlayer($winnerName);
		$loser = $this->selectPlayer($loserName);
		$change = $rCalc->calculateRatingsSolo($winner, $loser);
		$em = $this->getDoctrine()->getManager();
		$em->persist($winner);
		$em->persist($loser);
		$game = new Game();
		$game->setWinner($winner);
		$game->setLoser($loser);
//		$game->setChamp($champ);
		$game->setTime(new \DateTime());
		$game->setRatingChange($change);
		$em->persist($game);
		var_dump($data);
        $em->flush();
		if(!empty($data['Champ'])) {
			$repo= $this->getDoctrine()->getRepository('PingPongBundle:Champs');
			$champ = $repo->find((int)$data['Champ']);
		    $champGame = new TournamentGame();
            $champGame->setChamp($champ);
            $champGame->setGame($game);
            $em->persist($champGame);
            $em->flush();
        }


		return true;
	}

	protected function savePairsGame($winnerName1, $winnerName2, $loserName1, $loserName2) {
		if (empty($winnerName1) || empty($winnerName2) || empty($loserName1) || empty($loserName2)) {
			return false;
		}
		$rCalc = new EloRating();
		$winner1 = $this->selectPlayer($winnerName1);
		$winner2 = $this->selectPlayer($winnerName2);
		$loser1 = $this->selectPlayer($loserName1);
		$loser2 = $this->selectPlayer($loserName2);
		$change = $rCalc->calculateRatingsPairs($winner1, $winner2, $loser1, $loser2);
		$em = $this->getDoctrine()->getManager();
		$em->persist($winner1);
		$em->persist($winner2);
		$em->persist($loser1);
		$em->persist($loser2);
		$game = new GamePairs();
		$game->setWinner1($winner1);
		$game->setWinner2($winner2);
		$game->setLoser1($loser1);
		$game->setLoser2($loser2);
		$game->setTime(new \DateTime());
		$game->setRatingChange($change);
		$em->persist($game);
		$em->flush();
		return true;
	}

}
