<?php
/**
 * Created by PhpStorm.
 * User: Miegalius
 * Date: 2017-02-01
 * Time: 18:05
 */

namespace PingPongBundle\Controller;

use PingPongBundle\Entity\Champs;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;

class TournamentController extends DefaultController
{
    public function tournamentsAction(Request $request) {
        $twigArray = array();

        $fb = $this->createFormBuilder();
        $fb->add('champName', TextType::class, ['label_format' => '%name%',]);
        //$fb->add('gameCount', RangeType::class, ['label_format' => '%name%', 'attr' => ['min' => '1', 'max' => '5', 'value' => '1', 'class' => 'range']]);
        $fb->add('gameType', ChoiceType::class, ['label_format' => '%name%', 'choices' => array('Solo' => '0')]);
        $fb->add('addChamp', SubmitType::class, ['label_format' => '%name%',]);

        $form = $fb->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $champ = new Champs();
            $champ->setName($data['champName']);
            $champ->setGameType($data['gameType']);
            $champ->setStatus(Champs::STATUS_ACTIVE);
            $champ->setGameCount(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($champ);
            $em->flush();
            return $this->redirect($this->generateUrl('ping_pong_tournament'));
        }

        $champs = $this->getDoctrine()
            ->getRepository('PingPongBundle:Champs')
            ->findBy(array(), array('id' => 'ASC'));
        if (!empty($champs)) {
            $twigArray['champs'] = $champs;
        }

        $twigArray['form'] = $form->createView();
        return $this->render('PingPongBundle:Tournaments:tournaments.html.twig', $twigArray);
    }

    public function tournamentViewAction($id) {
        $champ = $this->getDoctrine()
            ->getRepository('PingPongBundle:Champs')
            ->find($id);
        if($champ->getGameType() == 0) {
            return $this->prepareSoloTournamentView($champ);
        } else {
            return $this->preparePairsTournamentView($champ);
        }
    }

    protected function prepareSoloTournamentView($champ) {
        $twigArray = [];
        $twigArray['id'] = $champ->getId();
        $twigArray['champ'] = $champ;
        $games = $this->getDoctrine()->getRepository('PingPongBundle:TournamentGame')->findAllGames($champ->getId());
        $players = $this->getDoctrine()->getRepository('PingPongBundle:TournamentGame')->findAllPlayersSolo($champ->getId());
        $twigArray['games'] = $games;
        $twigArray['players'] = $players;
        return $this->render('PingPongBundle:Tournaments:tournamentSoloView.html.twig', $twigArray);
    }

    protected function preparePairsTournamentView($champ) {
        return $this->render('PingPongBundle:Tournaments:tournamentPairsView.html.twig');
    }
}