/**
 * Created by Miegalius on 2017-01-29.
 */
$( "document" ).ready(function() {
    if(typeof playerListJs != 'undefined') {
        $("input[type='text']").autocomplete({
            source: playerListJs
        });
    }
});
