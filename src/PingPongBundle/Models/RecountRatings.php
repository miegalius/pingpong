<?php
/**
 * Created by PhpStorm.
 * User: Miegalius
 * Date: 2017-01-27
 * Time: 20:31
 */

namespace PingPongBundle\Models;

use Symfony\Component\Config\Definition\Exception\Exception;
use Doctrine\Bundle\DoctrineBundle\Registry;
use PingPongBundle\Entity\Game;
use PingPongBundle\Entity\GamePairs;
use PingPongBundle\Entity\Player;

class RecountRatings
{
    /**
     * @var Doctrine\Bundle\DoctrineBundle\Registry
     */
    protected $doctrine;

    /**
     * @var EloRating
     */
    protected $eloRating;

    /**
     * @var string
     */
    protected $gameType = 'solo';

    /**
     * @var string
     */
    protected $playerVar = 'ratingSolo';

    /**
     * @var string
     */
    protected $gameRepository = 'Game';

    /**
     * @var Game[]|GamePairs[]
     */
    protected $games;

    /**
     * @var Player[]
     */
    protected $players;

    public function __construct($doctrine, EloRating $eloRating) {
        $this->doctrine = $doctrine;
        $this->eloRating = $eloRating;
    }

    public function recountSolo() {
        /* @var Game[] $games */
        $games = $this->readData('Game');
        $players = $this->readPlayers();
        if(empty($players)) {
            return false;
        }
        foreach($players as $player) {
            /* @var Player $player */
            $player->setRatingSolo(1500);
            $this->players[$player->getId()] = $player;
        }
        $dm = $this->doctrine->getManager();
        foreach($games as $game) {
            $wId = $game->getWinnerId();
            $lId = $game->getLoserId();
            if(isset($this->players[$wId])) {
                $winner = $this->players[$wId];
            }
            if(isset($this->players[$lId])) {
                $loser = $this->players[$lId];
            }
            $change = $this->eloRating->calculateRatingsSolo($winner, $loser);
            $game->setRatingChange($change);
            $this->players[$lId] = $loser;
            $this->players[$wId] = $winner;
            $dm->persist($game);
        }
        $this->persistPlayers();
        $dm->flush();
        return true;
    }

    public function recountPairs() {
        /* @var GamePairs[] $games */
        $games = $this->readData('GamePairs');
        $players = $this->readPlayers();
        if(empty($players)) {
            return false;
        }
        foreach($players as $player) {
            /* @var Player $player */
            $player->setRatingPairs(1500);
            $this->players[$player->getId()] = $player;
        }
        $dm = $this->doctrine->getManager();
        foreach($games as $game) {
            $wId1 = $game->getWinnerId1();
            $wId2 = $game->getWinnerId2();
            $lId1 = $game->getLoserId1();
            $lId2 = $game->getLoserId2();
            if(isset($this->players[$wId1])) {
                $winner1 = $this->players[$wId1];
            }
            if(isset($this->players[$wId2])) {
                $winner2 = $this->players[$wId2];
            }
            if(isset($this->players[$lId1])) {
                $loser1 = $this->players[$lId1];
            }
            if(isset($this->players[$lId2])) {
                $loser2 = $this->players[$lId2];
            }
            $change = $this->eloRating->calculateRatingsPairs($winner1, $winner2, $loser1, $loser2);
            $game->setRatingChange($change);
            $this->players[$lId1] = $loser1;
            $this->players[$lId2] = $loser2;
            $this->players[$wId1] = $winner1;
            $this->players[$wId2] = $winner2;
            $dm->persist($game);
        }
        $this->persistPlayers();
        $dm->flush();
        return true;
    }


    protected function persistPlayers() {
        $dm = $this->doctrine->getManager();
        foreach($this->players as $player) {
            $dm->persist($player);
        }
        $dm->flush();
    }

    protected function readData($entityName) {
        try {
            $repository = $this->doctrine->getRepository('PingPongBundle:' . $entityName);
            $games = $repository->findBy(array('deleted' => '0'), array('time' => 'ASC'));
        } catch (Exception $e) {
            return false;
        }
        return $games;
    }

    protected function readPlayers() {
        $repository = $this->doctrine->getRepository('PingPongBundle:Player');
        $players = $repository->findAll();
        return $players;
    }
}