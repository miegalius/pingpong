<?php

namespace PingPongBundle\Models;

use PingPongBundle\Entity\Player;

class EloRating
{
    protected $kFactor = 100;

    protected function calculateRatings($winnerRating = 1500, $looserRating = 1500)
    {
        $winnerScore = 1 / (1 + (pow(10, ($winnerRating - $looserRating) / 400)));
        $looserScore = 1 / (1 + (pow(10, ($looserRating - $winnerRating) / 400)));
        $winnerRating = $this->calculateChange($winnerRating, $winnerScore, true);
        $looserRating = $this->calculateChange($looserRating, $looserScore, false);
        return array($winnerRating, $looserRating);
    }

    protected function calculatePairs($winner1 = 1500, $winner2 = 1500, $looser1 = 1500, $looser2 = 1500)
    {
        $winnersRatio = $winner1 + $winner2;
        $losersRatio = $looser1 + $looser2;
        list($newWinners, $newLosers) = $this->calculateRatings($winnersRatio, $losersRatio);
        $winnerCoef = ($newWinners - $winnersRatio) / $winnersRatio;
        $looserCoef = ($newLosers - $losersRatio) / $losersRatio;

        $newRatings = array(
            ($winner1 + ($winnerCoef * $winner2)), //Switched to increase lower rating more.
            ($winner2 + ($winnerCoef * $winner1)),
            ($looser1 + ($looserCoef * $looser1)),
            ($looser2 + ($looserCoef * $looser2)),
        );
        return $newRatings;
    }

    public function calculateRatingsSolo(Player &$winner, Player &$loser) {
        $rWinner = $winner->getRatingSolo();
        $rLoser = $loser->getRatingSolo();
        list($rWinnerNew, $rLoserNew) = $this->calculateRatings($rWinner, $rLoser);
        $winner->setRatingSolo($rWinnerNew);
        $loser->setRatingSolo($rLoserNew);
        $change = abs($rWinnerNew - $rWinner);
        return $change;
    }

    public function calculateRatingsPairs(Player &$winner1, Player &$winner2, Player &$loser1, Player &$loser2) {
        $rWinner1 = $winner1->getRatingPairs();
        $rWinner2 = $winner2->getRatingPairs();
        $rLoser1 = $loser1->getRatingPairs();
        $rLoser2 = $loser2->getRatingPairs();
        list($rWinnerNew1, $rWinnerNew2, $rLoserNew1, $rLoserNew2) = $this->calculatePairs($rWinner1, $rWinner2, $rLoser1, $rLoser2);
        $winner1->setRatingPairs($rWinnerNew1);
        $winner2->setRatingPairs($rWinnerNew2);
        $loser1->setRatingPairs($rLoserNew1);
        $loser2->setRatingPairs($rLoserNew2);
        $change =  abs(($rWinnerNew1 + $rWinnerNew2) - ($rWinner1 + $rWinner2));
        return $change;
    }

    protected function calculateChange($rating, $score, $win = true)
    {
        return $rating + ($this->kFactor * ($score - ($win ? 0 : 1)));
    }
}