<?php
/**
 * Created by PhpStorm.
 * User: Miegalius
 * Date: 2017-01-27
 * Time: 20:56
 */

namespace PingPongBundle\Entity;


class Game
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $winnerId;

    /**
     * @var integer
     */
    private $loserId;

    /**
     * @var \DateTime
     */
    private $time;

    /**
     * @var string
     */
    private $ratingChange = 0;

    /**
     * @var integer
     */
    private $deleted = 0;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $winner;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $loser;

	/**
	 * @var \PingPongBundle\Entity\Champs
	 */
	private $champ;

    public function __construct()
    {
        $this->time = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set winnerId
     *
     * @param integer $winnerId
     *
     * @return Game
     */
    public function setWinnerId($winnerId)
    {
        $this->winnerId = $winnerId;

        return $this;
    }

    /**
     * Get winnerId
     *
     * @return integer
     */
    public function getWinnerId()
    {
        return $this->winnerId;
    }

    /**
     * Set loserId
     *
     * @param integer $loserId
     *
     * @return Game
     */
    public function setLoserId($loserId)
    {
        $this->loserId = $loserId;

        return $this;
    }

    /**
     * Get loserId
     *
     * @return integer
     */
    public function getLoserId()
    {
        return $this->loserId;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return Game
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set ratingChange
     *
     * @param string $ratingChange
     *
     * @return Game
     */
    public function setRatingChange($ratingChange)
    {
        $this->ratingChange = $ratingChange;

        return $this;
    }

    /**
     * Get ratingChange
     *
     * @return string
     */
    public function getRatingChange()
    {
        return $this->ratingChange;
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return Game
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set winner
     *
     * @param \PingPongBundle\Entity\Player $winner
     *
     * @return Game
     */
    public function setWinner(\PingPongBundle\Entity\Player $winner = null)
    {
        $this->winner = $winner;

        return $this;
    }

    /**
     * Get winner
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getWinner()
    {
        return $this->winner;
    }

    /**
     * Set loser
     *
     * @param \PingPongBundle\Entity\Player $loser
     *
     * @return Game
     */
    public function setLoser(\PingPongBundle\Entity\Player $loser = null)
    {
        $this->loser = $loser;

        return $this;
    }

    /**
     * Get loser
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getLoser()
    {
        return $this->loser;
    }

	/**
	 * Set champ
	 *
	 * @param \PingPongBundle\Entity\Champs $champ
	 *
	 * @return Game
	 */
	public function setChamp(\PingPongBundle\Entity\Champs $champ = null)
	{
		$this->champ = $champ;

		return $this;
	}

	/**
	 * Get champ
	 *
	 * @return \PingPongBundle\Entity\Player
	 */
	public function getChamp()
	{
		return $this->champ;
	}
    /**
     * @var integer
     */
    private $champId;


    /**
     * Set champId
     *
     * @param integer $champId
     *
     * @return Game
     */
    public function setChampId($champId)
    {
        $this->champId = $champId;

        return $this;
    }

    /**
     * Get champId
     *
     * @return integer
     */
    public function getChampId()
    {
        return $this->champId;
    }
}
