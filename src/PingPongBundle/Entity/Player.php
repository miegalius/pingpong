<?php

namespace PingPongBundle\Entity;

/**
 * Player
 */
class Player
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var float
     */
    private $ratingSolo = 1500.00;

    /**
     * @var float
     */
    private $ratingPairs = 1500.00;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Player
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ratingSolo
     *
     * @param integer $ratingSolo
     *
     * @return Player
     */
    public function setRatingSolo($ratingSolo)
    {
        $this->ratingSolo = $ratingSolo;

        return $this;
    }

    /**
     * Get ratingSolo
     *
     * @return int
     */
    public function getRatingSolo()
    {
        return round($this->ratingSolo,2);
    }

    /**
     * Set ratingPairs
     *
     * @param integer $ratingPairs
     *
     * @return Player
     */
    public function setRatingPairs($ratingPairs)
    {
        $this->ratingPairs = $ratingPairs;

        return $this;
    }

    /**
     * Get ratingPairs
     *
     * @return int
     */
    public function getRatingPairs()
    {
        return round($this->ratingPairs,2);
    }
}
