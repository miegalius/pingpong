<?php

namespace PingPongBundle\Entity;

/**
 * TournamentGame
 */
class TournamentGame
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $champId;

    /**
     * @var integer
     */
    private $gameId;

    /**
     * @var \PingPongBundle\Entity\Champs
     */
    private $champ;

    /**
     * @var \PingPongBundle\Entity\Game
     */
    private $game;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set champId
     *
     * @param integer $champId
     *
     * @return TournamentGame
     */
    public function setChampId($champId)
    {
        $this->champId = $champId;

        return $this;
    }

    /**
     * Get champId
     *
     * @return integer
     */
    public function getChampId()
    {
        return $this->champId;
    }

    /**
     * Set gameId
     *
     * @param integer $gameId
     *
     * @return TournamentGame
     */
    public function setGameId($gameId)
    {
        $this->gameId = $gameId;

        return $this;
    }

    /**
     * Get gameId
     *
     * @return integer
     */
    public function getGameId()
    {
        return $this->gameId;
    }

    /**
     * Set champ
     *
     * @param \PingPongBundle\Entity\Champs $champ
     *
     * @return TournamentGame
     */
    public function setChamp(\PingPongBundle\Entity\Champs $champ = null)
    {
        $this->champ = $champ;

        return $this;
    }

    /**
     * Get champ
     *
     * @return \PingPongBundle\Entity\Champs
     */
    public function getChamp()
    {
        return $this->champ;
    }

    /**
     * Set game
     *
     * @param \PingPongBundle\Entity\Game $game
     *
     * @return TournamentGame
     */
    public function setGame(\PingPongBundle\Entity\Game $game = null)
    {
        $this->game = $game;

        return $this;
    }

    /**
     * Get game
     *
     * @return \PingPongBundle\Entity\Game
     */
    public function getGame()
    {
        return $this->game;
    }
}
