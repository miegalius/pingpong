<?php

namespace PingPongBundle\Entity;

/**
 * GamePairs
 */
class GamePairs
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $winnerId1;

    /**
     * @var integer
     */
    private $winnerId2;

    /**
     * @var integer
     */
    private $loserId1;

    /**
     * @var integer
     */
    private $loserId2;

    /**
     * @var \DateTime
     */
    private $time;

    /**
     * @var string
     */
    private $ratingChange = 0;

    /**
     * @var integer
     */
    private $deleted = 0;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $winner1;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $winner2;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $loser1;

    /**
     * @var \PingPongBundle\Entity\Player
     */
    private $loser2;

    public function __construct()
    {
        $this->time = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set winnerId1
     *
     * @param integer $winnerId1
     *
     * @return GamePairs
     */
    public function setWinnerId1($winnerId1)
    {
        $this->winnerId1 = $winnerId1;

        return $this;
    }

    /**
     * Get winnerId1
     *
     * @return integer
     */
    public function getWinnerId1()
    {
        return $this->winnerId1;
    }

    /**
     * Set winnerId2
     *
     * @param integer $winnerId2
     *
     * @return GamePairs
     */
    public function setWinnerId2($winnerId2)
    {
        $this->winnerId2 = $winnerId2;

        return $this;
    }

    /**
     * Get winnerId2
     *
     * @return integer
     */
    public function getWinnerId2()
    {
        return $this->winnerId2;
    }

    /**
     * Set loserId1
     *
     * @param integer $loserId1
     *
     * @return GamePairs
     */
    public function setLoserId1($loserId1)
    {
        $this->loserId1 = $loserId1;

        return $this;
    }

    /**
     * Get loserId1
     *
     * @return integer
     */
    public function getLoserId1()
    {
        return $this->loserId1;
    }

    /**
     * Set loserId2
     *
     * @param integer $loserId2
     *
     * @return GamePairs
     */
    public function setLoserId2($loserId2)
    {
        $this->loserId2 = $loserId2;

        return $this;
    }

    /**
     * Get loserId2
     *
     * @return integer
     */
    public function getLoserId2()
    {
        return $this->loserId2;
    }

    /**
     * Set time
     *
     * @param \DateTime $time
     *
     * @return GamePairs
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set ratingChange
     *
     * @param string $ratingChange
     *
     * @return GamePairs
     */
    public function setRatingChange($ratingChange)
    {
        $this->ratingChange = $ratingChange;

        return $this;
    }

    /**
     * Get ratingChange
     *
     * @return string
     */
    public function getRatingChange()
    {
        return round($this->ratingChange,2);
    }

    /**
     * Set deleted
     *
     * @param integer $deleted
     *
     * @return GamePairs
     */
    public function setDeleted($deleted)
    {
        $this->deleted = $deleted;

        return $this;
    }

    /**
     * Get deleted
     *
     * @return integer
     */
    public function getDeleted()
    {
        return $this->deleted;
    }

    /**
     * Set winner1
     *
     * @param \PingPongBundle\Entity\Player $winner1
     *
     * @return GamePairs
     */
    public function setWinner1(\PingPongBundle\Entity\Player $winner1 = null)
    {
        $this->winner1 = $winner1;

        return $this;
    }

    /**
     * Get winner1
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getWinner1()
    {
        return $this->winner1;
    }

    /**
     * Set winner2
     *
     * @param \PingPongBundle\Entity\Player $winner2
     *
     * @return GamePairs
     */
    public function setWinner2(\PingPongBundle\Entity\Player $winner2 = null)
    {
        $this->winner2 = $winner2;

        return $this;
    }

    /**
     * Get winner2
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getWinner2()
    {
        return $this->winner2;
    }

    /**
     * Set loser1
     *
     * @param \PingPongBundle\Entity\Player $loser1
     *
     * @return GamePairs
     */
    public function setLoser1(\PingPongBundle\Entity\Player $loser1 = null)
    {
        $this->loser1 = $loser1;

        return $this;
    }

    /**
     * Get loser1
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getLoser1()
    {
        return $this->loser1;
    }

    /**
     * Set loser2
     *
     * @param \PingPongBundle\Entity\Player $loser2
     *
     * @return GamePairs
     */
    public function setLoser2(\PingPongBundle\Entity\Player $loser2 = null)
    {
        $this->loser2 = $loser2;

        return $this;
    }

    /**
     * Get loser2
     *
     * @return \PingPongBundle\Entity\Player
     */
    public function getLoser2()
    {
        return $this->loser2;
    }
}
